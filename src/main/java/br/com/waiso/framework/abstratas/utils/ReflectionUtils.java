package br.com.waiso.framework.abstratas.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.abstratas.TipoMetodo;

/**
 * 
 * @author massuda.alex
 *
 */
public class ReflectionUtils {

	public static Method getMetodo(Class classe, TipoMetodo tipoMetodo, String atributo) {
		Method metodo = null;
		try {
			if (Classe.existe(classe)) {
				Class tipo = classe;
				for (Field campo : tipo.getDeclaredFields()) {
					if (campo.getName().equals(atributo)) {
						String primeiroCaracter = atributo.substring(0, 1).toUpperCase();
						String nomeMetodo = "";
						switch (tipoMetodo) {
						case GET:
							nomeMetodo = "get" + primeiroCaracter + atributo.substring(1);
							return tipo.getMethod(nomeMetodo, null);
						case SET:
							nomeMetodo = "set" + primeiroCaracter + atributo.substring(1);
							return tipo.getMethod(nomeMetodo, campo.getType());
						default:
							nomeMetodo = atributo;
							return tipo.getMethod(nomeMetodo, campo.getType());
						}
					}
				}
				metodo = tipo.getMethod(atributo, null);
			}
		} catch (NoSuchMethodException e) {
			boolean possuiHeranca = Classe.existe(classe.getSuperclass()) && (classe.getSuperclass() != Object.class);
			if (possuiHeranca) {
				return getMetodo(classe.getSuperclass(), tipoMetodo, atributo);
			} else {
				System.out.println("O Metodo nao existe ou esta com outra nomenclatura. Implemente o getter com a IDE Eclipse");
				e.printStackTrace();
			}
		}
		return metodo;
	}
	
	public static String getClasseGenerica (Field campo) {
//		return (Class<?>) ((ParameterizedType) classe.getGenericSuperclass())
//				.getActualTypeArguments()[0];
		return ((ParameterizedType)campo.getGenericType()).getActualTypeArguments()[0].getTypeName();
//		return campo.getType();
	}
	
	
	public static Class<?> getTipoCampoGenerico (Field campo) {
		return (Class)(((ParameterizedType)campo.getGenericType()).getActualTypeArguments()[0]);
	}
	

	public static Class<?> getTipoGenerico(List<?> lista) {
		Class<?> classeGenerica;
		Class<?> classe = lista.getClass();
		ParameterizedType genericSuperclass = (ParameterizedType) classe.getGenericSuperclass();
		Type actualTypeArgument = genericSuperclass.getActualTypeArguments()[0];
		classeGenerica = (Class<?>) ((ParameterizedType) classe.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		return classeGenerica;
	}

}
